package io.jakes.artifactory.manager.tests

import io.jakes.artifactory.manager.pub.client.ArtifactoryClient
import org.assertj.core.api.Assertions.assertThat
import org.junit.Ignore
import org.junit.Test

class ArtifactoryTests() {

    @Test
    @Ignore
    fun try_something() {
        val client = ArtifactoryClient.createClient(
            "",
            System.getenv("ARTIFACTORY_API_KEY"))

        val repositoriesResponse = client.listRepositories().execute()

        assertThat(repositoriesResponse.isSuccessful).isTrue()

        val repositories = repositoriesResponse.body()

        val allRepos = repositories.sortedBy { r -> r.key }
            .groupBy { r -> r.key }

        assertThat(allRepos).isNotNull

        val repo = allRepos.get("docker-myproject-local")!!.first();

        assertThat(repo).isNotNull()

        val dockerReposResponse = client.listDockerRepos(repo.key).execute()

        val dockerRepose = dockerReposResponse.body().repositories.groupBy { r -> r }

        val repoName = dockerRepose.get("myproject-api-dev")!!.first()

        val tagsResponse = client.listDockerTags(repo.key, repoName).execute()

        val tags = tagsResponse.body()

        assertThat(tags).isNotNull();

        val firstTag = tags.tags.first()

        val manifestResponse = client.getDockerManifest(repo.key, repoName, firstTag).execute()

        val manifest = manifestResponse.body()
        assertThat(manifest).isNotNull()

        val folderResponse = client.listFiles(repo.key).execute()

        val folder = folderResponse.body()

        val dockerManifests = folder.files.filter { f -> f.uri.endsWith("manifest.json") }
        val aManifest = dockerManifests.sortedBy { m -> m.created }.first()

        val fileInfo = client.getFile(repo.key, aManifest.uri).execute()

        assertThat(fileInfo.body()).isNotNull()

        if (firstTag != "latest") {
            val deleteResponse = client.deleteDockerByDigest(repo.key, repoName, firstTag).execute()
            assertThat(deleteResponse.isSuccessful).isTrue()
        }

    }
}
