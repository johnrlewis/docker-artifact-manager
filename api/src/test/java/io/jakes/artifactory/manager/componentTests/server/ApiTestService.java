package io.jakes.artifactory.manager.componentTests.server;

import io.jakes.artifactory.manager.ServiceApplication;
import io.jakes.artifactory.manager.ServiceConfiguration;
import io.jakes.artifactory.manager.bundles.ApiGuiceBundleProvider;
import com.google.inject.Injector;
import com.google.inject.Module;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.paradoxical.common.test.guice.ModuleUtils;
import io.paradoxical.common.test.guice.OverridableModule;

import java.util.List;
import java.util.concurrent.CountDownLatch;

public class ApiTestService extends ServiceApplication {
    private final CountDownLatch serviceReady = new CountDownLatch(1);
    private final TestApiGuiceBundleProvider testGuiceBundleProvier;

    public ApiTestService(final List<OverridableModule> modules) {
        testGuiceBundleProvier = new TestApiGuiceBundleProvider(modules);
    }

    @Override
    public void initialize(final Bootstrap<ServiceConfiguration> bootstrap) {
        bootstrap.addBundle(testGuiceBundleProvier.getBundle());
        super.initialize(bootstrap);
    }

    private static class TestApiGuiceBundleProvider extends ApiGuiceBundleProvider {
        private List<OverridableModule> overridableModules;

        public TestApiGuiceBundleProvider(final List<OverridableModule> overridableModules) {
            this.overridableModules = overridableModules;
        }

        @Override
        protected List<Module> getModules() {
            return ModuleUtils.mergeModules(super.getModules(), overridableModules);
        }
    }

    public void waitForReady() throws InterruptedException {
        serviceReady.await();
    }
    
    @Override
    public void run(final ServiceConfiguration config, final Environment env) throws Exception {
        super.run(config, env);

        env.lifecycle().addServerLifecycleListener(server -> serviceReady.countDown());
    }

    public Injector getInjector() {
        return testGuiceBundleProvier.getBundle().getInjector();
    }
}
