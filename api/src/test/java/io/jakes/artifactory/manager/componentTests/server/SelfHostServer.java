package io.jakes.artifactory.manager.componentTests.server;

import com.godaddy.logging.Logger;
import io.jakes.artifactory.manager.ServiceConfiguration;
import com.google.common.collect.ImmutableList;
import com.google.inject.Injector;
import io.paradoxical.common.test.guice.OverridableModule;
import io.paradoxical.common.test.web.runner.ServiceTestRunner;
import io.paradoxical.common.test.web.runner.ServiceTestRunnerConfig;
import lombok.Getter;

import java.net.URI;
import java.util.List;
import java.util.Random;

import static com.godaddy.logging.LoggerFactory.getLogger;

public class SelfHostServer implements AutoCloseable {
    private static final Logger logger = getLogger(SelfHostServer.class);

    @Getter
    private final List<OverridableModule> overridableModules;

    private static Random random = new Random();

    private ServiceTestRunner<ServiceConfiguration, ApiTestService> serviceConfigurationTestServiceServiceTestRunner;

    public SelfHostServer(List<OverridableModule> overridableModules) {
        this.overridableModules = overridableModules;
    }

    public void startApi(ServiceConfiguration configuration) {

        serviceConfigurationTestServiceServiceTestRunner =
                new ServiceTestRunner<>(ApiTestService::new,
                                        configuration,
                                        getNextPort());

        ServiceTestRunnerConfig serviceTestRunnerConfig = ServiceTestRunnerConfig.builder().applicationRoot("/").build();

        serviceConfigurationTestServiceServiceTestRunner.run(serviceTestRunnerConfig, ImmutableList.copyOf(overridableModules));
    }

    public void startApi() {
        startApi(getDefaultConfig());
    }

    @Override
    public void close() {
        try {
            stop();
        }
        catch (Exception e) {
            logger.warn(e, "Error stopping");
        }
    }

    public void stop() throws Exception {
        if (serviceConfigurationTestServiceServiceTestRunner != null) {
            serviceConfigurationTestServiceServiceTestRunner.close();
        }
    }

    protected static long getNextPort() {
        return random.nextInt(35000) + 15000;
    }

    public URI getBaseUri() {

        final String uri = String.format("http://localhost:%s/", serviceConfigurationTestServiceServiceTestRunner.getLocalPort());

        return URI.create(uri);
    }

    public Injector getInjector() {
        return serviceConfigurationTestServiceServiceTestRunner.getApplication().getInjector();
    }

    private ServiceConfiguration getDefaultConfig() {
        final ServiceConfiguration serviceConfiguration = new ServiceConfiguration();

        return serviceConfiguration;
    }

}