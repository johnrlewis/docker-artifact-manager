package io.jakes.artifactory.manager.bundles;

import io.jakes.artifactory.manager.ServiceApplication;
import io.jakes.artifactory.manager.ServiceConfiguration;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.hubspot.dropwizard.guice.GuiceBundle;
import com.netflix.governator.InjectorBuilder;

import java.util.List;

public abstract class BaseGuiceBundleProvider {

    private GuiceBundle<ServiceConfiguration> bundle;

    public final synchronized GuiceBundle<ServiceConfiguration> getBundle() {
        if (bundle == null) {
            bundle = buildGuiceBundle();
        }

        return bundle;
    }

    protected BaseGuiceBundleProvider() {
    }

    public static Injector getInjectorFactory(final List<Module> modules) {
        return InjectorBuilder.fromModules(modules).createInjector();
    }

    protected abstract List<Module> getModules();

    private GuiceBundle<ServiceConfiguration> buildGuiceBundle() {
        final GuiceBundle.Builder<ServiceConfiguration> builder = GuiceBundle.newBuilder();

        builder.enableAutoConfig(ServiceApplication.class.getPackage().getName())
               .setConfigClass(ServiceConfiguration.class)
               .setInjectorFactory((stage, modules) -> getInjectorFactory(modules));

        getModules().stream().forEach(builder::addModule);

        final GuiceBundle<ServiceConfiguration> guiceBundle = builder.build();

        return guiceBundle;
    }
}
