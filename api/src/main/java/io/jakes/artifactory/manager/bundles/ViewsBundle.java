package io.jakes.artifactory.manager.bundles;

import io.jakes.artifactory.manager.ServiceConfiguration;
import io.dropwizard.ConfiguredBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import io.dropwizard.views.ViewRenderer;
import io.dropwizard.views.mustache.MustacheViewRenderer;

import java.util.ArrayList;
import java.util.List;

public class ViewsBundle implements ConfiguredBundle<ServiceConfiguration> {

    @Override
    public void run(final ServiceConfiguration configuration, final Environment environment) throws Exception {

    }

    @Override
    public void initialize(final Bootstrap<?> wildcardBootstrap) {
        final Bootstrap<ServiceConfiguration> bootstrap = (Bootstrap<ServiceConfiguration>) wildcardBootstrap;

        List<ViewRenderer> viewRenders = new ArrayList<>();

        viewRenders.add(new MustacheViewRenderer());

        bootstrap.addBundle(new ViewBundle<>(viewRenders));
    }
}
