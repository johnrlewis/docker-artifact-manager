package io.jakes.artifactory.manager.bundles;

import io.jakes.artifactory.manager.ServiceConfiguration;
import io.dropwizard.ConfiguredBundle;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class DefaultAssetsBundle implements ConfiguredBundle<ServiceConfiguration> {

    @Override
    public void run(final ServiceConfiguration configuration, final Environment environment) throws Exception {
    }

    @Override
    public void initialize(final Bootstrap<?> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets", "/assets", null, "default-assets"));
    }
}
