package io.jakes.artifactory.manager;

import com.godaddy.logging.Logger;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import lombok.Getter;

import java.util.Optional;
import java.util.function.Function;

import static com.godaddy.logging.LoggerFactory.getLogger;

public final class EnvironmentVariables {
    public static final EnvironmentVariables Current = new EnvironmentVariables(ImmutableMap.copyOf(System.getenv()));
    private static final Logger logger = getLogger(EnvironmentVariables.class);

    private final ImmutableMap<String, String> environmentVars;

    @Getter
    private final HttpOptions http = new HttpOptions(this);

    private EnvironmentVariables(final ImmutableMap<String, String> environmentVars) {
        this.environmentVars = environmentVars;
    }

    private Optional<String> getSetting(final String settingName) {
        return getSetting(settingName, Function.identity());
    }

    private <T> Optional<T> getSetting(final String settingName, final Function<String, T> converter) {
        final String var = environmentVars.getOrDefault(settingName, null);

        if (Strings.isNullOrEmpty(var)) {
            return Optional.empty();
        }

        return Optional.ofNullable(converter.apply(var));
    }

    public Optional<String> getConfigurationEnvironment() {
        return getSetting("ENV_CONF");
    }

    public Optional<String> getAppRoot() {
        return getSetting("APP_ROOT");
    }

    public static final class HttpOptions {

        private final EnvironmentVariables variables;

        private HttpOptions(final EnvironmentVariables variables) {
            this.variables = variables;
        }

        public Optional<Integer> getApiPort() {
            return variables.getSetting("API_HTTP_PORT", Integer::valueOf);
        }


        public Optional<Integer> getAdminPort() {
            return variables.getSetting("ADMIN_HTTP_PORT", Integer::valueOf);
        }
    }
}
