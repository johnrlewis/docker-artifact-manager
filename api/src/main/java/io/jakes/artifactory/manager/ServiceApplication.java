package io.jakes.artifactory.manager;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.godaddy.logging.Logger;
import io.jakes.artifactory.manager.commands.ApiServerCommand;
import io.jakes.artifactory.manager.handlers.ParameterHandlerProvider;
import io.jakes.artifactory.manager.serialization.JacksonJsonMapper;
import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.jersey.jackson.JacksonMessageBodyProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.paradoxical.common.web.web.filter.CorrelationIdFilter;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.function.BiConsumer;

import static com.godaddy.logging.LoggerFactory.getLogger;


public class ServiceApplication extends Application<ServiceConfiguration> {

    private static final Logger logger = getLogger(ServiceApplication.class);

    public static void main(final String[] args) throws Exception {
        DateTimeZone.setDefault(DateTimeZone.UTC);

        ServiceApplication serviceApplication = new ServiceApplication();

        try {
            serviceApplication.run(args);
        }
        catch (Throwable ex) {
            ex.printStackTrace();

            System.exit(1);
        }
    }

    @Override
    public void initialize(final Bootstrap<ServiceConfiguration> bootstrap) {

        bootstrap.addCommand(new ApiServerCommand(this));

        final SubstitutingSourceProvider provider =
                new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(),
                                               new EnvironmentVariableSubstitutor(false));

        bootstrap.setConfigurationSourceProvider(provider);
    }


    @Override
    public void run(final ServiceConfiguration config, final Environment env) throws Exception {

        ArrayList<BiConsumer<ServiceConfiguration, Environment>> run = new ArrayList<>();

        run.add(this::configureJson);

        run.add(this::configureLogging);

        run.add(this::configureFilters);

        run.stream().forEach(configFunction -> configFunction.accept(config, env));
    }

    private void configureFilters(final ServiceConfiguration serviceConfiguration, final Environment env) {
        env.jersey().register(new CorrelationIdFilter());
        env.jersey().register(new ParameterHandlerProvider());
    }

    private void configureLogging(final ServiceConfiguration serviceConfiguration, final Environment environment) {
        Logs.setup();
    }

    protected void configureJson(final ServiceConfiguration config, final Environment environment) {
        ObjectMapper mapper = new JacksonJsonMapper().getMapper();

        JacksonMessageBodyProvider jacksonBodyProvider =
                new JacksonMessageBodyProvider(mapper, environment.getValidator());

        environment.jersey().register(jacksonBodyProvider);
    }
}
