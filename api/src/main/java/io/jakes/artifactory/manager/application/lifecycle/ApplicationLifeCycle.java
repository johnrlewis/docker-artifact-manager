package io.jakes.artifactory.manager.application.lifecycle;

import com.godaddy.logging.Logger;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import io.dropwizard.lifecycle.Managed;
import lombok.AllArgsConstructor;

import static com.godaddy.logging.LoggerFactory.getLogger;


@Singleton
@SuppressWarnings("unused")
@AllArgsConstructor(onConstructor = @__(@Inject))
public class ApplicationLifeCycle implements Managed {
    private static final Logger logger = getLogger(ApplicationLifeCycle.class);

    @Override
    public void start() {
        logger.info("Starting pusher controller");

    }

    @Override
    public void stop() throws Exception {
        logger.dashboard("STOPPED core app logic");
    }
}
