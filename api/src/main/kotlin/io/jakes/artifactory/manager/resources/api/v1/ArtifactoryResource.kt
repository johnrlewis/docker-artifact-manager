package io.jakes.artifactory.manager.resources.api.v1

import com.godaddy.logging.LoggerFactory
import com.google.common.base.CharMatcher
import com.google.common.base.Splitter
import com.google.inject.Inject
import io.jakes.artifactory.manager.ArtifactoryClientFactory
import io.jakes.artifactory.manager.pub.client.ArtifactoryClient
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import java.time.Clock
import java.time.OffsetDateTime
import javax.ws.rs.Consumes
import javax.ws.rs.DELETE
import javax.ws.rs.DefaultValue
import javax.ws.rs.GET
import javax.ws.rs.HeaderParam
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.QueryParam
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/api/v1/artifactory/")
@Api(tags = arrayOf("Artifactory"), description = "Artifactory Docker Management Api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
class ArtifactoryResource @Inject constructor(private val artifactoryClientFactory: ArtifactoryClientFactory) {
    private val logger = LoggerFactory.getLogger(ArtifactoryResource::class.java)

    @GET
    @ApiOperation(value = "List all artifactory repositories")
    @ApiResponses(value = *arrayOf(ApiResponse(code = 201, message = "Ok"),
                                   ApiResponse(code = 500, message = "Internal Server Error")))
    @Path("/repositories")
    fun listAllRepositories(@HeaderParam("Authorization") apiKey: String): Response {

        val artifactoryClient = artifactoryClientFactory.createClient(apiKey);

        val repositoriesResponse = artifactoryClient.listRepositories().execute()

        if (repositoriesResponse.isSuccessful.not()) {
            return Response.serverError().build();
        }

        val repos = repositoriesResponse.body()

        return Response.ok()
            .entity(repos)
            .build()
    }

    @GET
    @ApiOperation(value = "List all docker images in repository")
    @ApiResponses(value = *arrayOf(ApiResponse(code = 200, message = "Ok"),
                                   ApiResponse(code = 500, message = "Internal Server Error")))
    @Path("/repositories/{repoName}/images")
    fun listDockerImages(
        @PathParam("repoName") repoName: String,
        @HeaderParam("Authorization") apiKey: String): Response {

        val artifactoryClient = artifactoryClientFactory.createClient(apiKey);

        val dockerImagesResponse = artifactoryClient.listDockerRepos(repoName).execute()

        if (dockerImagesResponse.isSuccessful.not()) {
            return Response.serverError().build();
        }

        val repos = dockerImagesResponse.body().repositories

        return Response.ok()
            .entity(repos)
            .build()
    }

    @GET
    @ApiOperation(value = "List all docker tags sorted by last modified date time")
    @ApiResponses(value = *arrayOf(ApiResponse(code = 200, message = "Ok"),
                                   ApiResponse(code = 500, message = "Internal Server Error")))
    @Path("/repositories/{repoName}/images/{imageName}/tags")
    fun listDockerImageTags(
        @PathParam("repoName") repoName: String,
        @PathParam("imageName") imageName: String,
        @HeaderParam("Authorization") apiKey: String): Response {

        val artifactoryClient = artifactoryClientFactory.createClient(apiKey);

        val dockerImagesResponse = artifactoryClient.listDockerTags(repoName, imageName).execute()

        if (dockerImagesResponse.isSuccessful.not()) {
            return Response.serverError().build();
        }

//        val tags = dockerImagesResponse.body().tags

        val matcher = CharMatcher.`is`('/')

        var files = artifactoryClient.listFiles(repoName, imageName).execute()
        val manifests = files.body().files.filter { f -> f.uri.endsWith("manifest.json") }
            .map { f -> Splitter.on(matcher).split(matcher.trimLeadingFrom(f.uri)).first().to(f.lastModified) }

        val tags = manifests.sortedBy { m -> m.second }.map { m -> m.first }.reversed()

        return Response.ok()
            .entity(tags)
            .build()
    }

    @DELETE
    @ApiOperation(value = "Deletes docker images")
    @ApiResponses(value = *arrayOf(ApiResponse(code = 202, message = "Deleting"),
                                   ApiResponse(code = 500, message = "Internal Server Error")))
    @Path("/repositories/{repoName}/images/{imageName}/tags")
    fun cleanDockerImageTags(
        @PathParam("repoName") repoName: String,
        @PathParam("imageName") imageName: String,
        @DefaultValue("0") @QueryParam("keep") keep: Int,
        @DefaultValue("0") @QueryParam("olderThanDays") olderThanDays: Long,
        @DefaultValue("false") @QueryParam("try") isTestRun: Boolean,
        @QueryParam("exclude") exclude: Set<String>,
        @QueryParam("excludeContains") excludeContains: Set<String>,
        @HeaderParam("Authorization") apiKey: String): Response {

        val artifactoryClient = artifactoryClientFactory.createClient(apiKey);

        val dockerImagesResponse = artifactoryClient.listDockerTags(repoName, imageName).execute()

        if (!dockerImagesResponse.isSuccessful) {
            return Response.serverError().build();
        }

        val exclusionSet = setOf("latest") + exclude

        val matcher = CharMatcher.`is`('/')
        val splitter = Splitter.on(matcher)

        data class TagInfo(val tag: String, val lastModified: OffsetDateTime) { }

        val files = artifactoryClient.listFiles(repoName, imageName).execute()
        val manifests = files.body().files
            .filter { it.uri.endsWith("manifest.json") }
            .map { TagInfo(splitter.split(matcher.trimLeadingFrom(it.uri)).first(),
                           it.lastModified.toOffsetDateTime()) }

        var filtered = manifests.filter { it.tag !in exclusionSet }

        for (ec in excludeContains)
            filtered = filtered.filter { !it.tag.contains(ec) }

        val olderThan = OffsetDateTime.now(Clock.systemUTC()).minusDays(olderThanDays)

        val tags =
                filtered
                .sortedBy { it.lastModified } // ascending by last modified date time
                .dropLast(keep) // last because its ascending
                .takeWhile { olderThan.isAfter(it.lastModified) }
                .map { it.tag }

        if (!isTestRun) {
            tags.forEach { tag -> artifactoryClient.deleteDockerByDigest(repoName, imageName, tag).execute() }
        }

        return Response.accepted()
            .entity(mapOf("deletedTags" to tags))
            .build()
    }
}
