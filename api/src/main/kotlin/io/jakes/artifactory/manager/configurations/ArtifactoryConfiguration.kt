package io.jakes.artifactory.manager.configurations


data class ArtifactoryConfiguration(
    var url: String? = null,
    var apiKey: String? = null) {
}
