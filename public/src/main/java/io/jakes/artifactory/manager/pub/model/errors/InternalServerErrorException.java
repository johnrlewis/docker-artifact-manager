package io.jakes.artifactory.manager.pub.model.errors;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class InternalServerErrorException extends WebApplicationException {
    public InternalServerErrorException(final Throwable e) {
        super(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
             .entity(
                 ErrorEntity.builder()
                 .code(ErrorEntity.getErrorCodeInternalServerError())
                 .message(ErrorEntity.getErrorMessageInternalServerError())
                 .stackTrace(ErrorEntity.convertStackTraceToString(e))
                 .build()).build());
    }
}
