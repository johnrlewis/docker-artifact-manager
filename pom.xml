<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <prerequisites>
        <maven>3.0.0</maven>
    </prerequisites>
    <groupId>io.jakes</groupId>
    <artifactId>artifactory-manager</artifactId>
    <packaging>pom</packaging>
    <version>1.0${revision}</version>
    <name>Artifactory Manager</name>
    <description>Artifactory Manager Root Project</description>

    <modules>
        <module>api</module>
        <module>public</module>
    </modules>

    <scm>
        <connection>scm:git:git@github.com:group/project.git</connection>
        <developerConnection>scm:git:git@github.com:group/project.git</developerConnection>
        <tag>HEAD</tag>
    </scm>

    <distributionManagement>
        <repository>
            <id>bintray-jakes-maven</id>
            <name>jakes-maven</name>
            <url>https://api.bintray.com/maven/jakes/maven/[PACKAGE_NAME]/;publish=1</url>
        </repository>
    </distributionManagement>

    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <properties>
        <!-- Project Settings -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <kotlin.version>1.0.2</kotlin.version>
        <java.version>1.8</java.version>
        <revision>-SNAPSHOT</revision>

        <!-- stupid slow site build -->
        <dependency.locations.enabled>false</dependency.locations.enabled>

        <!-- Build settings -->
        <skipTests>false</skipTests>
        <build.skipTests>${skipTests}</build.skipTests>

        <!-- build.git.branch is created below -->
        <build.branch>${build.git.branch}</build.branch>

        <!-- build.git.sha is created below -->
        <git_sha>${build.git.sha}</git_sha>
        <buildTag>${project.version}-${git_sha}</buildTag>

        <!-- Docker settings -->
        <docker.skipBuild>false</docker.skipBuild>
        <docker.skipTag>false</docker.skipTag>
        <docker.forceTags>true</docker.forceTags>
        <docker.pushImage>false</docker.pushImage>
        <docker.pushImageTags>true</docker.pushImageTags>
        <docker.pullOnBuild>false</docker.pullOnBuild>
        <docker.host>${env.DOCKER_HOST}</docker.host>

        <docker.registry>https://registry.hub.docker.com</docker.registry>

        <docker.image.repo>jakeswenson</docker.image.repo>

        <docker.image.tag.version>${project.version}</docker.image.tag.version>
        <docker.image.tag.splitter>-</docker.image.tag.splitter>
        <docker.image.tag.end>${build.branch}</docker.image.tag.end>
        <docker.image.tag>${docker.image.tag.version}</docker.image.tag>

        <docker.image.name.classifier></docker.image.name.classifier>
        <docker.image.name>${project.artifactId}${docker.image.name.classifier}</docker.image.name>

        <docker.image>${docker.image.repo}/${docker.image.name}:${docker.image.tag}</docker.image>

        <!-- Library Versions -->
        <lombok.version>1.16.8</lombok.version>

        <paradoxical.common.testing.version>1.0</paradoxical.common.testing.version>
        <paradoxical.common.version>2.0</paradoxical.common.version>
        <paradoxical.swagger.version>1.1</paradoxical.swagger.version>
        <paradoxical.jersey.extras.version>1.0</paradoxical.jersey.extras.version>
        <paradoxical.rabbitmq.version>1.0</paradoxical.rabbitmq.version>

        <swagger.version>1.5.6</swagger.version>

        <godaddy.logging.version>1.2.1</godaddy.logging.version>

        <dropwizard.version>0.9.2</dropwizard.version>
        <dropwizard.template.config.version>1.1.0</dropwizard.template.config.version>
        <dropwizard.metrics.version>0.9.2</dropwizard.metrics.version>
        <dropwizard.guice.version>0.8.4.0</dropwizard.guice.version>

        <guava.version>19.0</guava.version>

        <guice.assistedInject.version>4.0</guice.assistedInject.version>

        <retrofit.version>2.0.2</retrofit.version>
        <retrofit.converter.jackson.version>${retrofit.version}</retrofit.converter.jackson.version>

        <apache.commons.lang3.version>3.3.2</apache.commons.lang3.version>
        <apache.commons.collections.version>4.0</apache.commons.collections.version>

        <hibernate.validator.version>5.2.4.Final</hibernate.validator.version>

        <jackson.lombok.version>1.1</jackson.lombok.version>
        <jackson.version>2.7.4</jackson.version>
        <jackson.datatype.version>${jackson.version}</jackson.datatype.version>

        <netflix.governator.core.version>1.11.0</netflix.governator.core.version>
        <asm.version>5.1</asm.version>
        <jcabi.manifests.version>1.1</jcabi.manifests.version>

        <assertj.version>3.3.0</assertj.version>
        <junit.version>4.12</junit.version>
        <podam.version>5.4.0.RELEASE</podam.version>

        <!-- Maven Plugin versions -->
        <maven.plugin.compiler.version>3.0</maven.plugin.compiler.version>
        <maven.plugin.exec.version>1.2.1</maven.plugin.exec.version>
        <maven.plugin.surefire.version>2.19.1</maven.plugin.surefire.version>
        <maven.plugin.cobertura.version>2.6</maven.plugin.cobertura.version>
        <maven.plugin.shade.version>2.4.3</maven.plugin.shade.version>
        <maven.plugin.source.version>3.0.0</maven.plugin.source.version>
        <maven.plugin.buildnumber.version>1.4</maven.plugin.buildnumber.version>
        <maven.plugin.javadoc.version>2.10.3</maven.plugin.javadoc.version>
        <maven.plugin.findbugs.version>3.0.3</maven.plugin.findbugs.version>
        <maven.plugin.checkstyle.version>2.17</maven.plugin.checkstyle.version>
        <maven.plugin.docker.version>0.4.9</maven.plugin.docker.version>
        <maven.plugin.jar.version>2.6</maven.plugin.jar.version>
        <maven.plugin.release.version>2.5.1</maven.plugin.release.version>
        <maven.plugin.deploy.version>2.8.2</maven.plugin.deploy.version>
        <maven.plugin.dep.scm-provider-gitexe.version>1.9.4</maven.plugin.dep.scm-provider-gitexe.version>
        <maven.plugin.scm.version>1.9.4</maven.plugin.scm.version>
        <maven.plugin.release.version>3.0.0</maven.plugin.release.version>
        <maven.plugin.install.version>2.5.2</maven.plugin.install.version>

        <!--reporting plugin versions-->
        <reporting.maven-surefire-report-plugin.version>2.18.1</reporting.maven-surefire-report-plugin.version>
        <reporting.maven-jxr-plugin.version>2.5</reporting.maven-jxr-plugin.version>
        <reporting.cobertura-maven-plugin.version>2.7</reporting.cobertura-maven-plugin.version>
        <reporting.findbugs-maven-plugin.version>3.0.0</reporting.findbugs-maven-plugin.version>
        <reporting.maven-pmd-plugin.version>5.2.1</reporting.maven-pmd-plugin.version>
        <reporting.maven-project-info-reports-plugin.version>2.9</reporting.maven-project-info-reports-plugin.version>
        <maven.reporting.plugin.surefire.version>2.14</maven.reporting.plugin.surefire.version>
    </properties>

    <profiles>
        <profile>
            <id>feature-build</id>
            <properties>
                <build.git.branch>${env.GIT_BRANCH}</build.git.branch>
                <build.git.sha>${env.GIT_COMMIT}</build.git.sha>
                <docker.image.tag>${build.git.sha}</docker.image.tag>
                <docker.image.name.classifier>-dev</docker.image.name.classifier>
            </properties>
            <activation>
                <property>
                    <name>featureBuild</name>
                    <value>true</value>
                </property>
            </activation>
        </profile>
        <profile>
            <id>local-deploy</id>
            <properties>
                <docker.image.tag>dev</docker.image.tag>
                <docker.host>unix:///var/run/docker.sock</docker.host>
            </properties>
        </profile>
        <profile>
            <id>osx-local-build</id>
            <properties>
                <docker.image.tag>dev</docker.image.tag>
                <docker.host>unix:///var/run/docker.sock</docker.host>
                <docker.pushImageTags>false</docker.pushImageTags>
                <docker.image.name.classifier></docker.image.name.classifier>
            </properties>
            <activation>
                <property>
                    <name>osx</name>
                    <value>true</value>
                </property>
            </activation>
        </profile>
    </profiles>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>io.paradoxical</groupId>
                <artifactId>rabbitmq</artifactId>
                <version>${paradoxical.rabbitmq.version}</version>
            </dependency>

            <dependency>
                <groupId>io.paradoxical</groupId>
                <artifactId>jackson-lombok</artifactId>
                <version>${jackson.lombok.version}</version>
            </dependency>

            <dependency>
                <groupId>io.paradoxical</groupId>
                <artifactId>jersey-extras</artifactId>
                <version>${paradoxical.jersey.extras.version}</version>
            </dependency>

            <dependency>
                <groupId>io.paradoxical</groupId>
                <artifactId>dropwizard-swagger</artifactId>
                <version>${paradoxical.swagger.version}</version>
            </dependency>

            <dependency>
                <groupId>com.netflix.governator</groupId>
                <artifactId>governator-core</artifactId>
                <version>${netflix.governator.core.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>com.fasterxml.jackson.core</groupId>
                        <artifactId>jackson-databind</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>io.paradoxical</groupId>
                <artifactId>common</artifactId>
                <version>${paradoxical.common.version}</version>
            </dependency>

            <dependency>
                <groupId>com.fasterxml.jackson.datatype</groupId>
                <artifactId>jackson-datatype-jdk8</artifactId>
                <version>${jackson.datatype.version}</version>
            </dependency>

            <dependency>
                <groupId>com.fasterxml.jackson.datatype</groupId>
                <artifactId>jackson-datatype-guava</artifactId>
                <version>${jackson.datatype.version}</version>
            </dependency>

            <dependency>
                <groupId>com.fasterxml.jackson.datatype</groupId>
                <artifactId>jackson-datatype-jsr310</artifactId>
                <version>${jackson.datatype.version}</version>
            </dependency>

            <dependency>
                <groupId>com.godaddy</groupId>
                <artifactId>logging</artifactId>
                <version>${godaddy.logging.version}</version>
            </dependency>

            <dependency>
                <groupId>com.google.guava</groupId>
                <artifactId>guava</artifactId>
                <version>${guava.version}</version>
            </dependency>

            <dependency>
                <groupId>com.google.inject.extensions</groupId>
                <artifactId>guice-assistedinject</artifactId>
                <version>${guice.assistedInject.version}</version>
            </dependency>

            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-core</artifactId>
                <version>${dropwizard.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>io.dropwizard.metrics</groupId>
                        <artifactId>metrics-core</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-validation</artifactId>
                <version>${dropwizard.version}</version>
            </dependency>

            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-client</artifactId>
                <version>${dropwizard.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>io.dropwizard.metrics</groupId>
                        <artifactId>metrics-core</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-views-mustache</artifactId>
                <version>${dropwizard.version}</version>
            </dependency>

            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-views</artifactId>
                <version>${dropwizard.version}</version>
            </dependency>

            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-assets</artifactId>
                <version>${dropwizard.version}</version>
            </dependency>
            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-testing</artifactId>
                <version>${dropwizard.version}</version>
            </dependency>

            <dependency>
                <groupId>io.dropwizard</groupId>
                <artifactId>dropwizard-metrics-graphite</artifactId>
                <version>${dropwizard.metrics.version}</version>
            </dependency>

            <dependency>
                <groupId>org.hibernate</groupId>
                <artifactId>hibernate-validator</artifactId>
                <version>${hibernate.validator.version}</version>
            </dependency>

            <dependency>
                <groupId>io.paradoxical</groupId>
                <artifactId>common.test</artifactId>
                <version>${paradoxical.common.testing.version}</version>
                <scope>test</scope>
                <exclusions>
                    <exclusion>
                        <groupId>org.slf4j</groupId>
                        <artifactId>slf4j-log4j12</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.slf4j</groupId>
                        <artifactId>log4j-over-slf4j</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.hibernate</groupId>
                        <artifactId>hibernate-validator</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>com.hubspot.dropwizard</groupId>
                <artifactId>dropwizard-guice</artifactId>
                <version>${dropwizard.guice.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>io.dropwizard</groupId>
                        <artifactId>dropwizard-core</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>io.dropwizard</groupId>
                        <artifactId>dropwizard-jackson</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>io.dropwizard</groupId>
                        <artifactId>dropwizard-jersey</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>io.dropwizard</groupId>
                        <artifactId>dropwizard-jetty</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>io.dropwizard</groupId>
                        <artifactId>dropwizard-servlets</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>io.dropwizard</groupId>
                        <artifactId>dropwizard-lifecycle</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>${apache.commons.lang3.version}</version>
            </dependency>

            <dependency>
                <groupId>io.swagger</groupId>
                <artifactId>swagger-core</artifactId>
                <version>${swagger.version}</version>
            </dependency>

            <dependency>
                <groupId>io.swagger</groupId>
                <artifactId>swagger-jersey2-jaxrs</artifactId>
                <version>${swagger.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>org.glassfish.jersey.core</groupId>
                        <artifactId>jersey-common</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>org.glassfish.jersey.containers</groupId>
                        <artifactId>jersey-container-servlet-core</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>com.squareup.retrofit2</groupId>
                <artifactId>retrofit</artifactId>
                <version>${retrofit.version}</version>
            </dependency>

            <dependency>
                <groupId>com.squareup.retrofit2</groupId>
                <artifactId>converter-jackson</artifactId>
                <version>${retrofit.converter.jackson.version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>com.fasterxml.jackson.core</groupId>
                        <artifactId>jackson-databind</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>org.ow2.asm</groupId>
                <artifactId>asm</artifactId>
                <version>${asm.version}</version>
            </dependency>

            <dependency>
                <groupId>com.jcabi</groupId>
                <artifactId>jcabi-manifests</artifactId>
                <version>${jcabi.manifests.version}</version>
                <optional>true</optional>
            </dependency>

            <dependency>
                <groupId>org.assertj</groupId>
                <artifactId>assertj-core</artifactId>
                <version>${assertj.version}</version>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>${junit.version}</version>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>uk.co.jemos.podam</groupId>
                <artifactId>podam</artifactId>
                <version>${podam.version}</version>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>${lombok.version}</version>
                <optional>true</optional>
                <scope>provided</scope>
            </dependency>

            <dependency>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-stdlib</artifactId>
                <version>${kotlin.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
        </dependency>

        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
        </dependency>

        <dependency>
            <groupId>uk.co.jemos.podam</groupId>
            <artifactId>podam</artifactId>
        </dependency>

        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-stdlib</artifactId>
        </dependency>
    </dependencies>

    <build>
        <resources>
            <resource>
                <directory>${project.basedir}</directory>
                <includes>
                    <include>pom.xml</include>
                </includes>
                <targetPath>META-INF/maven/${project.groupId}/${project.artifactId}/</targetPath>
                <filtering>true</filtering>
            </resource>
        </resources>

        <pluginManagement>
            <plugins>
                <plugin>
                    <artifactId>maven-scm-plugin</artifactId>
                    <version>${maven.plugin.scm.version}</version>
                    <configuration>
                        <tag>${project.artifactId}-${project.version}</tag>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>com.spotify</groupId>
                    <artifactId>docker-maven-plugin</artifactId>
                    <version>${maven.plugin.docker.version}</version>

                    <configuration>
                        <skipDockerBuild>${docker.skipBuild}</skipDockerBuild>
                        <pullOnBuild>${docker.pullOnBuild}</pullOnBuild>
                        <pushImage>${docker.pushImage}</pushImage>
                        <pushImageTag>${docker.pushImageTags}</pushImageTag>
                        <forceTags>${docker.forceTags}</forceTags>
                        <dockerHost>${docker.host}</dockerHost>

                        <skipDockerTag>true</skipDockerTag>

                        <serverId>docker.hub</serverId>
                        <registryUrl>${docker.registry}</registryUrl>

                        <imageName>${docker.image}</imageName>
                        <imageTags>
                            <imageTag>latest</imageTag>
                            <imageTag>${docker.image.tag}</imageTag>
                        </imageTags>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>${maven.plugin.source.version}</version>
                    <executions>
                        <execution>
                            <id>attach-sources</id>
                            <goals>
                                <goal>jar-no-fork</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${maven.plugin.compiler.version}</version>
                    <configuration>
                        <source>${java.version}</source>
                        <target>${java.version}</target>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>${maven.plugin.jar.version}</version>
                    <configuration>
                        <archive>
                            <manifestEntries>
                                <SCM-Revision>${buildTag}</SCM-Revision>
                                <SCM-Branch>${build.branch}</SCM-Branch>
                            </manifestEntries>
                        </archive>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-shade-plugin</artifactId>
                    <version>${maven.plugin.shade.version}</version>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>${maven.plugin.release.version}</version>
                    <configuration>
                        <autoVersionSubmodules>true</autoVersionSubmodules>
                    </configuration>
                    <dependencies>
                        <dependency>
                            <groupId>org.apache.maven.scm</groupId>
                            <artifactId>maven-scm-provider-gitexe</artifactId>
                            <version>${maven.plugin.dep.scm-provider-gitexe.version}</version>
                        </dependency>
                    </dependencies>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-project-info-reports-plugin</artifactId>
                    <version>${reporting.maven-project-info-reports-plugin.version}</version>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>${maven.plugin.deploy.version}</version>
                    <configuration>
                        <deployAtEnd>true</deployAtEnd>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>${maven.plugin.surefire.version}</version>
                    <configuration>
                        <!--the -XX:-UseSplitVerifier option slows class loading.
                        It does not affect security, runtime performance or functionality.
                        http://stackoverflow.com/questions/15253173/how-safe-is-it-to-use-xx-usesplitverifier -->
                        <argLine>-XX:-UseSplitVerifier</argLine>
                        <includes>
                            <include>%regex[.*]</include>
                        </includes>
                        <excludes>
                            <exclude>%regex[.*.json]</exclude>
                        </excludes>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>buildnumber-maven-plugin</artifactId>
                    <version>${maven.plugin.buildnumber.version}</version>
                    <executions>
                        <execution>
                            <phase>validate</phase>
                            <goals>
                                <goal>create</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <doCheck>false</doCheck>
                        <doUpdate>false</doUpdate>

                        <buildNumberPropertyName>build.git.sha</buildNumberPropertyName>
                        <scmBranchPropertyName>build.git.branch</scmBranchPropertyName>

                        <useLastCommittedRevision>true</useLastCommittedRevision>
                        <getRevisionOnlyOnce>true</getRevisionOnlyOnce>
                        <shortRevisionLength>7</shortRevisionLength>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-javadoc-plugin</artifactId>
                    <version>${maven.plugin.javadoc.version}</version>
                    <configuration>
                        <failOnError>false</failOnError>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-report-plugin</artifactId>
                    <version>${maven.reporting.plugin.surefire.version}</version>
                    <configuration>
                        <aggregate>true</aggregate>
                    </configuration>
                    <executions>
                        <execution>
                            <id>generate-test-report</id>
                            <phase>test</phase>
                            <goals>
                                <goal>report-only</goal>
                            </goals>
                        </execution>
                        <execution>
                            <id>generate-integration-test-report</id>
                            <phase>integration-test</phase>
                            <goals>
                                <goal>failsafe-report-only</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>

                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>findbugs-maven-plugin</artifactId>
                    <version>${maven.plugin.findbugs.version}</version>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <version>${maven.plugin.checkstyle.version}</version>
                    <executions>
                        <execution>
                            <id>validate</id>
                            <phase>validate</phase>
                            <configuration>
                                <configLocation>checkstyle.xml</configLocation>
                                <encoding>UTF-8</encoding>
                                <!--<includeTestSourceDirectory>true</includeTestSourceDirectory>-->
                                <consoleOutput>true</consoleOutput>
                                <includeResources>false</includeResources>
                                <includeTestResources>false</includeTestResources>
                            </configuration>
                            <goals>
                                <goal>check</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>

                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>cobertura-maven-plugin</artifactId>
                    <version>${maven.plugin.cobertura.version}</version>
                    <dependencies>
                        <dependency>
                            <groupId>org.ow2.asm</groupId>
                            <artifactId>asm</artifactId>
                            <version>${asm.version}</version>
                        </dependency>
                    </dependencies>
                    <configuration>
                        <check>
                            <haltOnFailure>true</haltOnFailure>

                            <!-- Per-class thresholds -->
                            <lineRate>0</lineRate>
                            <branchRate>0</branchRate>

                            <!-- Project-wide thresholds -->
                            <totalLineRate>83</totalLineRate>
                            <totalBranchRate>10</totalBranchRate>
                        </check>
                        <aggregate>true</aggregate>
                        <quiet>false</quiet>
                        <skip>${build.skipTests}</skip>
                        <instrumentation>
                        </instrumentation>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>${maven.plugin.release.version}</version>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-install-plugin</artifactId>
                    <version>${maven.plugin.install.version}</version>
                </plugin>

                <plugin>
                    <groupId>org.jetbrains.kotlin</groupId>
                    <artifactId>kotlin-maven-plugin</artifactId>
                    <version>${kotlin.version}</version>
                    <inherited>true</inherited>
                    <configuration>
                        <sourceDirs>
                            <sourceDir>${project.basedir}/src/main/kotlin</sourceDir>
                        </sourceDirs>
                    </configuration>
                    <executions>
                        <execution>
                            <id>kotlin-compile</id>
                            <phase>process-sources</phase>
                            <goals>
                                <goal>compile</goal>
                            </goals>
                            <configuration>
                                <defaultSourceDirs>
                                    <sourceDir>${project.basedir}/src/main/kotlin</sourceDir>
                                </defaultSourceDirs>
                            </configuration>
                        </execution>
                        <execution>
                            <id>kotlin-test-compile</id>
                            <phase>process-test-sources</phase>
                            <goals>
                                <goal>test-compile</goal>
                            </goals>
                            <configuration>
                                <defaultSourceDirs>
                                    <sourceDir>${project.basedir}/src/test/kotlin</sourceDir>
                                </defaultSourceDirs>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
            </plugin>

            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>buildnumber-maven-plugin</artifactId>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <executions>
                    <execution>
                        <id>replace-pom-placeholder</id>
                        <phase>package</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <resources>
                                <resource>
                                    <directory>${project.basedir}</directory>
                                    <includes>
                                        <include>pom.xml</include>
                                    </includes>
                                    <filtering>true</filtering>
                                </resource>
                            </resources>
                            <outputDirectory>${project.build.directory}/pom-install-deploy-fix</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-install-plugin</artifactId>
                <executions>
                    <execution>
                        <id>overwrite-pom</id>
                        <phase>install</phase>
                        <goals>
                            <goal>install-file</goal>
                        </goals>
                        <configuration>
                            <packaging>pom</packaging>
                            <file>target/pom-install-deploy-fix/pom.xml</file>
                            <pomFile>target/pom-install-deploy-fix/pom.xml</pomFile>
                            <version>${project.version}</version>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <executions>
                    <execution>
                        <id>overwrite-pom</id>
                        <phase>deploy</phase>
                        <goals>
                            <goal>deploy-file</goal>
                        </goals>
                        <configuration>
                            <packaging>pom</packaging>
                            <file>target/pom-install-deploy-fix/pom.xml</file>
                            <pomFile>target/pom-install-deploy-fix/pom.xml</pomFile>
                            <version>${project.version}</version>
                            <repositoryId>release.repo</repositoryId>
                            <uniqueVersion>false</uniqueVersion>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <configuration>
                    <configLocation>checkstyle.xml</configLocation>
                </configuration>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>checkstyle</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
        </plugins>
    </reporting>
</project>
